/*section_3 */

    const tabs = document.querySelector('.tabs');

    const tabsContent = document.querySelector('.tabs-content');

    tabs.addEventListener('click', (event) =>{
    const li = event.target.closest('li');

    let active = document.querySelector(".active");

    if(active){
        active.classList.remove('active');
        findsContent(li)
    }
    li.classList.add('active');
    
    });

    function findsContent(li){
        for (let i of tabsContent.children) {
        if (i.dataset.tab === li.dataset.tab) {
            li.classList.remove('active');
            i.classList.add('active');     
        }else{
            li.classList.add('active');
            i.classList.remove('active');
        }
        }
    };

    /*section_5*/

    //№1//

    const tabsAll = document.querySelector('#tabs_All');
    const tabsContentAll = document.querySelector('#tabs-content_All');

    tabsAll.addEventListener('click', (event) =>{
    const liAll = event.target.closest('li');

    let activeAll = document.querySelector(".mainActive");

    if(activeAll){
        activeAll.classList.remove('mainActive');
        Content(liAll)
    }
    liAll.classList.add('mainActive');
    });

    function Content(liAll){
        for (let i of tabsContentAll.children) {
        if (i.dataset.tab === liAll.dataset.tab) {
            liAll.classList.remove('mainActive');
            i.classList.add('mainActive');     
        }else{
            liAll.classList.add('mainActive');
            i.classList.remove('mainActive');
        }
        }
    };

    //№2//

    const btn = document.querySelector(".button_Load_More");
    const load = document.querySelector('#preloader');

    btn.addEventListener('click', () => {
        load.classList.toggle('active')
        btn.style.display = 'none';
    
        setTimeout(() => {
        load.classList.toggle('active')
        loadMore();
        }, 2000)
    });

    function loadMore () {
        const items = document.querySelectorAll(".tabs-contents_All_div .load_more_hidden");
        if(items){
            [...items].forEach(item =>{
                item.classList.remove("load_more_hidden");
            })
        }
    }; 
    
    /*section_7*/

        /* 1.1 */

    const tabsEllipse = document.querySelector('.section_7_photo');
    
    const tabsContentEllipse = document.querySelector('#tab-contender_ellipse');
    
    tabsEllipse.addEventListener('click', (event) =>{
    const liEllipse = event.target.closest('li');
    
    let activeEllipse = document.querySelector(".tabs-titles_ellipse.mainsActive");

    if(activeEllipse){
        activeEllipse.classList.remove('mainsActive');
        pageContents(liEllipse)
    }
    liEllipse.classList.add('mainsActive');
    });

    function pageContents (liEllipse){
        for (let i of tabsContentEllipse.children) {
        if (i.dataset.tab === liEllipse.dataset.tab) {
            liEllipse.classList.remove('mainsActive');
            i.classList.add('mainsActive');     
        }else{
            liEllipse.classList.add('mainsActive');
            i.classList.remove('mainsActive');
        }
        }
    };

        /* 1.2 */
    
    const slider = document.querySelector('.slider');

    slider.addEventListener('click', (event) => {
      const btn = event.target.closest('button');
    
      if (!btn) {
        return;
      }
    
      const direction = btn.classList.contains('section_7_button_left') ? 'left' : 'right';
    
      const items = [...tabsEllipse.children];
    
      const currentIndex = items.findIndex((item) =>
        item.classList.contains('mainsActive')
      );
      const nextItem = direction === 'left' ? items[currentIndex - 1] : items[currentIndex + 1];
    
      if (nextItem) {
        nextItem.click();
      }
    });


    

        